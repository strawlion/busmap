// nextBusService - Acts as a high level wrapper around the nextbus API. Callers currently can request route
//                  and vehicle positional data for San Francisco, though with minor tweaking this could be made
//                  a generic service to provide next bus data for any support locale

app.service('nextBusService', function($http) {

    var routeDataUrl       = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni';
    var busDataUrl         = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=sf-muni&t=0'; //TODO: check if we need to use PST datetime



    // Get all of San Francisco's bus data for the past 15 minutes
    function requestBusData(callback) {

        $http({method: 'GET', url: busDataUrl}).
          success(function(data, status, headers, config) {
            
            var busData = $.xml2json(data).vehicle;


            if (callback) {
                callback(busData);
            }

          });

    }


    // Get all of San Francisco's route data
    function requestRouteData(callback) {

        var routeUrl = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni';          // TODO: Too much data? Maybe stream from server
        $http({method: 'GET', url: routeDataUrl}).
          success(function(data, status, headers, config) {

            var routeData = $.xml2json(data).route;

            
            if (callback) {
                callback(routeData);
            }

        });


    }


        // Service object we are exposing
        var serviceObject = {
            requestBusData: requestBusData,
            requestRouteData: requestRouteData
        }

        return serviceObject;
});