// dataFormattingService - This is a general purpose service for formatting data. As of right now
//                         each exposed function serves a specific purpose, but this could be generecized
//                         to make this service more extensible.

app.service('dataFormattingService', function() {

        // Returns an array of the buses to draw based on the newly selected routes
       function createBusArrayFromSelectedRoutes(routes, selectedRoutes) {

            var buses = [];

            // Format buses data
             for (var i = 0; i < routes.length; i++) {
                var currentRoute = routes[i];

                // If the current route is selected, draw it
                var isRouteSelected =$.inArray(currentRoute, selectedRoutes);
                if (isRouteSelected === -1) { 
                    continue;
                }

                for (bus in currentRoute.buses) {
                    buses.push(currentRoute.buses[bus]);
                }
             }

             return buses;
        }

    // Updates the entries in the passed routeData
    function formatRouteData(routeData) {

        var formattedRouteData = {};

        for (var i = 0; i < routeData.length; i++) {
            var currentRoute = routeData[i];

            var formattedRoute = {};
            formattedRoute.color = '#' + currentRoute.color;
            formattedRoute.title = currentRoute.title;
            formattedRoute.tag   = currentRoute.tag.replace(' ', ''); // Remove periods from tag
            formattedRoute.paths = formatPathData(currentRoute.path);
            formattedRoute.buses = {};

           formattedRouteData[currentRoute.tag] = formattedRoute;
        }

        return formattedRouteData;
    }

    // Converts lat long path coordinates to LineString for easier drawing
    function formatPathData(paths) {

            var newPaths = [];

           for(var i = 0; i < paths.length; i++) {
                var currentPoints = paths[i].point;

                var newLineString = {};
                newLineString.type = "LineString";
                newLineString.coordinates = [];


                for (var j = 0; j < currentPoints.length; j++) {
                    var currentPoint = currentPoints[j];
                    newLineString.coordinates.push([currentPoint.lon, currentPoint.lat]);
                }

                newPaths.push(newLineString);
            }

            return newPaths;

        }



        // Service object we are exposing
        var serviceObject = {
            createBusArrayFromSelectedRoutes: createBusArrayFromSelectedRoutes,
            formatRouteData: formatRouteData
        }


        return serviceObject;
});