// indexController - Controls interaction with the home page (and currently the only page). 
//                   A lot of this functionality could be absorbed into the busMap directive
//                   and make the code simpler if we wanted to have the route selection and 
//                   legend be native to the directive.

app.controller('indexController', function($scope, dataFormattingService, nextBusService) {


  $scope.isFinishedLoading = false;

  $scope.selectedBus    = undefined; 
  $scope.selectedRoutes = [];
	$scope.routeData      = {};

  // Tracks time since last vehicle data request
  $scope.secondsSinceLastRequest = 0;

    // Make a request using the nextBusService for current vehicle positional data
    function getBusData() {

      nextBusService.requestBusData(function(newBusData) {


          for (var i = 0; i < newBusData.length; i++) {
              var currentBus  = newBusData[i];
              var busToUpdate = $scope.routeData[currentBus.routeTag].buses[currentBus.id];

              $scope.routeData[currentBus.routeTag].buses[currentBus.id] = currentBus; // Update the data
              $scope.secondsSinceLastRequest = 0;
          }

      });

    }



    function getRouteData(callback) {

      nextBusService.requestRouteData(function(routeData) {
        $scope.routeData = dataFormattingService.formatRouteData(routeData);


        if (callback) {
          callback();
        }

      });

    }


    function incrementTimer() {
      $scope.secondsSinceLastRequest += 1;
      $scope.$apply();
    }

        $scope.add = function(numberOne, numberTwo){
                    return parseInt(numberOne, 10) + parseInt(numberTwo, 10);
                  };


    // Calls all functions needed for page initialization.
    // Will get route data, bus data, and set bus data to be
    // updated every 15  seconds
      function initializePage() {

        getRouteData(function() {

          getBusData();
          $scope.isFinishedLoading = true;
        });

        setInterval(getBusData, 15000);    // Request new bus positional data every 15 seconds
        setInterval(incrementTimer, 1000); // Keep seconds since last data request
      }

      initializePage();


});
