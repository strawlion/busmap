// busMapDirective - This directive is currently used to draw a map of San Francisco and plot the vehicle positional data 
//                   passed as an attribute. Currently the indexController will request new data every 15 seconds and this 
//                   directive will be redraw itself due to a watch on this data. We could make this directive work with any 
//                   GeoJSON map by creating a generic mapService which would load the requested map of any supported locale.
//                   An alternative to using a data watch would be to create a chartRedrawService that explicitly tells certain 
//                   charts to redraw.

app.directive('busMap', function (dataFormattingService) {
    
    return {
        restrict: 'E'
        , replace: true
        , transclude: false
        , scope: {
            routeData: '=',
            selectedRoutes: '=',
            selectedBus: '='
        }
        , template: '<div class="busMap"></div>'
        , link: busMapLink

    };


// Handles the drawing of the San Francisco scene
function busMapLink ($scope, $element, $attrs) {

    var width = '100%',
        height = '100%';
    
    var mapScale = 200000;
    var sanFranciscoLongLat = [-122.4767, 37.7833];

    var projection = d3.geo.mercator()
        .center(sanFranciscoLongLat)
        .scale(mapScale);

    var svg = d3.select($element[0]).append("svg")
                                    .style('width', width)
                                    .style('height', height)
                                    .call(d3.behavior.zoom()
                                            .translate(projection.translate())
                                            .scale(projection.scale())
                                            .on('zoom', zoomHandler));// Zoom Behaviour

    var g = svg.append("g")
                .attr('transform', "translate(0,0) scale(1)");


    var path = d3.geo.path()
                     .projection(projection);
                     

    // Handles panning and zooming in/out with the mouse
    function zoomHandler() {

        var translatePosition = d3.event.translate;
        var zoomLevel = (d3.event.scale / mapScale);

        g.attr("transform", 
            "translate("+ translatePosition +") " +
            "scale("+zoomLevel+")"
        );
    }


    // Load the data and draw the map
    drawSanFrancisco();

      // Fire any time the route/bus data changes
      $scope.$watch('routeData', function(newRouteData, oldRouteData) {

          // Don't draw buses if these conditions aren't met
          if (!newRouteData || $.isEmptyObject(newRouteData) ) {
            return;
          }

          drawSelectedData();

      }, true);


      // Fire any time the selected routes change
      $scope.$watch('selectedRoutes', function (newSelectedRoutes, oldSelectedRoutes) {

          // Don't draw buses if these conditions aren't met
          if (newSelectedRoutes === undefined) {
              return;
          }


              drawSelectedData();

      }, true); // End watch
  


      function drawSelectedData() {

        var newRouteArray = d3.values($scope.routeData);

        drawRoutePaths(newRouteArray);
        drawBuses(newRouteArray);

      }


        // Draw buses for selected routes
        function drawBuses(routes){
            // Update our bus data
            var busData = dataFormattingService.createBusArrayFromSelectedRoutes(routes, $scope.selectedRoutes);

            var buses = g.selectAll('circle.bus')
                           .data(busData, function(d) { return(d.id); }); // Match by id instead of index.
     


            // Update Existing Bus Data
              buses.transition()
                   .attr('r', 2)
                .duration(2500)
                   .attr("transform", function(bus) { 
                          return "translate(" + projection([bus.lon, bus.lat]) + ")";
                  });

          

            // Plot New Bus Data
            buses.enter()
                .append("circle")
                .attr('class', 'bus')
                .attr("r", 7)
                .attr('fill', function(bus) { 
                    return $scope.routeData[bus.routeTag].color; 
                })
                .attr('stroke', function(bus) {
                  var fillColor   = $scope.routeData[bus.routeTag].color;
                  var strokeColor = d3.rgb(fillColor).darker(1); 
                  
                  return strokeColor;
                })
                .attr('stroke-width', .8)
                .attr("transform", function(bus) { 
                    return "translate(" + projection([bus.lon, bus.lat]) + ")";
                 })
                .on('click', function(bus) {
                  $scope.selectedBus = bus;
                  $scope.$parent.$apply();
                })
                .transition() // transition entering bus data
                  .duration(750)
                  .attr('r', 2);
      

            // Remove Old Bus Data
            buses.exit()
                 .remove();

        }

          
        // TODO: Switch to show/hide instead of redrawing
        // Draw route paths for selectedRoutes
        function drawRoutePaths(routes){

           // Draw route path for each selected route
           routes.forEach(function(currentRoute) {

                  var isRouteSelected = $.inArray(currentRoute, $scope.selectedRoutes);
                  if (isRouteSelected === -1){
                     $('.route' + currentRoute.tag).remove();
                      return;
                  }

                    var paths = g.selectAll('path.route' + currentRoute.tag)
                                     .data(currentRoute.paths)

                    // Plot New Route Data
                    paths.enter()
                      .append('path')
                      .attr('class', 'route' + currentRoute.tag)
                      .attr("d", path)
                      .attr('stroke-width', .5)
                      .attr('stroke', currentRoute.color)
                      .attr('fill', 'none');


           });

        }


    // Load GeoJSON of San Francisco and draw it
    function drawSanFrancisco() {

      // Freeways
      d3.json("sfmaps/freeways.json", function(error, topology) {
          
          
          g.selectAll("path.freeway")
            .data(topology.features)
          .enter()
            .append("path")
            .attr('class', 'freeway')
            .attr("d", path)
            .attr('stroke-width', 1)
            .attr('stroke', 'black')
            .attr('fill', 'none');

          console.log('Freeways Drawn');
      });

      // Streets
      d3.json("sfmaps/streets.json", function(error, topology) {
          
          g.selectAll("path.street")
            .data(topology.features)
          .enter()
            .append("path")
            .attr('class', 'street')
            .attr("d", path)
            .attr('stroke-width', '.2')
            .attr('stroke', 'black')
            .attr('fill', 'none')

          console.log('Streets Drawn');
      });

      // Arteries
      d3.json("sfmaps/arteries.json", function(error, topology) {
          
          g.selectAll("path.artery")
            .data(topology.features)
          .enter()
            .append("path")
            .attr('class', 'artery')
            .attr("d", path)
            .attr('stroke-width', .5)
            .attr('stroke', 'black')
            .attr('fill', 'none');

          console.log('Arteries Drawn');
      });

      // Neighborhoods
      d3.json("sfmaps/neighborhoods.json", function(error, topology) {
          

          g.selectAll("path.neighborhood")
            .data(topology.features)
          .enter()
            .append("path")
            .attr('class', 'neighborhood')
            .attr("d", path)
            .attr('stroke-width', '.2')
            .attr('stroke', 'black')
            .attr('fill', 'none');

          console.log('Neighborhoods Drawn');
      });

    }


}










});

