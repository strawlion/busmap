// TODO: Refactor EVERYTHING
// TODO: create objects for buses so that we can update

var currentUnixTimePST = (new Date().getTime()) - 15000;// Hack to get all bus data; // TODO: Convert to PST
var routeUrl           = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni';          // TODO: Too much data? Maybe stream from server
var routeListUrl       = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=sf-muni';
var vehicleLocationUrl = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=sf-muni&t=0'; //TODO: check if we need to use PST datetime


var colorScale = d3.scale.category20();


var width = 1600,
    height = 900;


var sanFranciscoLongLat = [-122.4167/*° W*/, 37.7833/*° N*/];
var projection = d3.geo.mercator()
    .center(sanFranciscoLongLat)
    .scale(150000);

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

var path = d3.geo.path()
                 .projection(projection);

// TODO: d3.geo.path().projection(d3.geo.mercator())); ??????/


var g = svg.append("g");
          /*.attr('width', width)
          .attr('height', height)
          .attr('transform', "translate(0,500)")*/



// TODO: Separate map drawing from data flowing in
// Draw the map
updateBusData(); // TODO: Weird. Change to render chart?
setInterval(updateBusData, 15000);

// Draw sanfrancisco

// TODO: change to use single path element for speed (.select('path'))
//TODO: Use resource loader
// load and display the World
d3.json("sfmaps/freeways.json", function(error, topology) {
    
    
    svg.selectAll("path.freeway")
      .data(topology.features)
    .enter()
      .append("path")
      .attr('class', 'freeway')
      .attr("d", path);

    console.log('Freeways Drawn');
});

// load and display the World
d3.json("sfmaps/streets.json", function(error, topology) {
    
    svg.selectAll("path.street")
      .data(topology.features)
    .enter()
      .append("path")
      .attr('class', 'street')
      .attr("d", path);

    console.log('Streets Drawn');
});

// load and display the World
d3.json("sfmaps/arteries.json", function(error, topology) {
    
    svg.selectAll("path.artery")
      .data(topology.features)
    .enter()
      .append("path")
      .attr('class', 'artery')
      .attr("d", path);

    console.log('Arteries Drawn');
});

// load and display the World
d3.json("sfmaps/neighborhoods.json", function(error, topology) {
    

    svg.selectAll("path.neighborhood")
      .data(topology.features)
    .enter()
      .append("path")
      .attr('class', 'neighborhood')
      .attr("d", path)
      .attr('stroke', 'black')
      .attr('stroke-width', '2')
      .attr('fill', 'none');

    console.log('Neighborhoods Drawn');
});



/*
// Draw Stops
$.get(routeUrl, function(xmlDocument) {


    var routeData = $.xml2json(xmlDocument).route;

    var routes = svg.selectAll("circle.bus")
                   .data(routeData); 

    
    // Draw Stops for each route 
    routes.each(function(route) {

        var stopsForThisRoute = route.stop;

            var stops = svg.selectAll('circle.stop')
                           .data(stopsForThisRoute)
                        .enter()
                            .append('circle')
                            .attr('class', 'stop')
                            .attr('r', 3)
                            .attr('fill', '#' + route.color)
                           .attr("transform", function(stop) { 
                                return "translate(" + projection([stop.lon, stop.lat]) + ")";
                            });



        // DRAW PATHS

        var pathsForThisRoute = formatPathData(route.path);

            var paths = svg.selectAll('path.path')
                           .data(pathsForThisRoute)
                        .enter()
                            .append('path')
                            .attr('class', 'path')
                            .attr('fill', '#' + route.color)
                            .attr("d", path);


    });

    console.log('Stops Drawn');
    console.log('Paths Drawn');

});
*/



function formatPathData(paths) {

    var newPaths = [];

   for(var i = 0; i < paths.length; i++) {
        var currentPoints = paths[i].point;
        

        var newLineString = {};
        newLineString.type = "LineString";
        newLineString.coordinates = [];


        for (var j = 0; j < currentPoints.length; j++) {
            var currentPoint = currentPoints[j];
            newLineString.coordinates.push([currentPoint.lon, currentPoint.lat]);
        }

        newPaths.push(newLineString);
    }

    return newPaths;

}




var busData = {};
function renderChart(){



    /*var vehicleLocationData = [];

    for (var i = 0; i < vehicleLocations.vehicle.length; i++) {
      vehicleLocationData.push([vehicleLocations.vehicle[i].lon, vehicleLocations.vehicle[i].lat]);
    }*/



    // TESTING
        var buses = svg.selectAll("circle.bus")
                       .data(d3.values(busData)); //TODO: look into d3.values
        
        // update existing bus data
          buses.transition()
       //        .duration(15000)
               .attr("transform", function(vehicle) { 
                    return "translate(" + projection([vehicle.lon, vehicle.lat]) + ")";
                });

        // Plot new bus data
        buses.enter()
            .append("circle")
            .attr('class', 'bus')
            .attr("r", 2)
            .attr('fill', function(vehicle) { return colorScale(vehicle.routeTag); } )
            .attr("transform", function(vehicle) { 
                return "translate(" + projection([vehicle.lon, vehicle.lat]) + ")";
             })
            .append("svg:title") // TOOLTIP
            .text(function(vehicle) { return vehicle.routeTag; });
          



}











// TODO: Is this even needed?
function updateRouteData(routes) {

    return routes;
   /* var routeData = {};

      for (var i = 0; i < routes.length; i++) {
      var currentRoute  = routes[i];
      var routeToUpdate = routeData[routes.id];

      routeData[currentRoute.title] = currentRoute;*/
  

}

// TODO: Inefficient, update the existing object
// Format the incoming data into our more useful vehicleData object
function updateBusData() {
    
    // Draw buses
    $.get(vehicleLocationUrl, function(xmlDocument) {
      var newBusData = $.xml2json(xmlDocument).vehicle;


      for (var i = 0; i < newBusData.length; i++) {
          var currentBus  = newBusData[i];
          var busToUpdate = busData[currentBus.id];

          // If vehicleData does not contain this vehicle, or the data is more recent, use this data
          if (busToUpdate === undefined || busToUpdate.secsSinceReport > currentBus.secsSinceReport) {
              busData[currentBus.id] = currentBus;
          }

      }
    });

    renderChart();// TODO: Change this location
}

/*// Converts Route Data to GEOJSON
function formatRouteData(routeData) {

  var geoJSON = {};
  geoJson.features
}*/

/*d3.xhr('http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni&r=N', 'xml', function(error, data) {
  console.log('success');
});*/




/*

NextBus Inc. 2013© Page 21 of 22 Rev. 1.22
April 4, 2013
Command "vehicleLocations"
To obtain a list of vehicle locations that have changed since the last time the vehicleLocations command was used, use the "vehicleLocations" command. 
The agency is specified by the "a" parameter in the query string. The tag for the agency as obtained from the agencyList command should be used. 
The route is specified by the "r" parameter. The tag for the route is obtained using the routeList command. 
The "t" parameter specifies the last time that was returned by the vehicleLocations command. 
The time in msec since the 1970 epoch time. If you specify a time of 0, then data for the last 15 minutes is provided.

The format of the command for obtaining vehicle location is:
http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=<agency_tag>&r=<route tag>&t=<epoch time in msec>
Attributes contained in the vehicle locations output are:
 id (string) – Identifier of the vehicle. It is often but not always numeric.
 routeTag (string) - Specifies the ID of the route the vehicle is currently associated with.
 dirTag (string) - Specifies the ID of the direction that the vehicle is currently on. The direction ID is usually the same as a trip pattern ID, but is very different from the tripTag. A direction or trip pattern ID specifies the configuration for a trip. It can be used multiple times for a block assignment. But a tripTag identifies a particular trip within a block assignment.
 lat/lon – specify the location of the vehicle.
 secsSinceReport (int) – How many seconds since the GPS location was actually recorded. It should be noted that sometimes a GPS report can be several minutes old.
 predictable (boolean) – Specifies whether the vehicle is currently predictable.
 heading (int) – Specifies the heading of the vehicle in degrees. Will be a value between 0 and 360. A negative value indicates that the heading is not currently available.
 speedKmHr (double) – Specifies GPS based speed of vehicle.


*/