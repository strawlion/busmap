# BusMap

This is just a quick project I put together to work on improving my skills with D3 and the integration of D3 with AngularJS.

I tailored the code to work specifically with the San Francisco Bus and Rail system, but with minor tweaking the services and directive could be made generic to any bus system supported by the NextBus API.